# cllm
The mail goals of this project are to create a version of `CLM` that:
1. doesn't require a writeable `$CLEAN_HOME`, this in turn allows packaging Clean for traditional Unix distributions.
2. is easier to maintain and better documented, but is not as portable as the existing `clm`.
3. can be used with alternative code generators and possibly compilers.

This version will not:
1. Use the caching functionality of `cocl`.
2. Be compatible with the original `clm`.
3. Claim to be as effecient as the original `clm`.

## Usage
Go to your project directory and do: ``cllm init`` this creates a template `cllm.toml` file. Edit this file to your
liking. Calling `cllm build` will subsequently build the project.

## Instalation
Run `cargo install cllm`
