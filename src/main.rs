use std::{string::String, path::Path};
use std::path::PathBuf;

use build::build_project;
use clap::{Parser, Subcommand};

mod build;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    module: String,

    #[arg(short, long, action = clap::ArgAction::Count)]
    verbose: u8,

    #[arg(short = 'I', long, action = clap::ArgAction::Append)]
    include: Vec<PathBuf>,

    #[arg(short, long, default_value = "a.out")]
    output: PathBuf,
}

fn main() -> Result<(), String> {
    let args = Args::parse();
    build_project(args.include, args.module, args.output.as_path());
    Ok(())
}
