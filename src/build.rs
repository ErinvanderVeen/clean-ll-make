use std::io::prelude::*;
use std::io::{BufRead, BufReader};
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use std::string::String;
use std::*;
use std::{
    collections::BTreeSet,
    fs::{create_dir, File},
};
use std::{collections::HashMap, env};

use regex::Regex;

/// Read the generated abc_file to fetch all dependencies
fn add_dependencies<P: AsRef<Path>>(
    abc_file: P,
    clean_dependencies: &mut BTreeSet<String>,
    cc_dependencies: &mut BTreeSet<PathBuf>,
    compiled: &mut HashMap<String, bool>,
) {
    let file = File::open(&abc_file).expect("Could not read compiler result");
    let lines = BufReader::new(file).lines();
    for line in lines {
        match line {
            Ok(line) => {
                if line == ".endinfo" {
                    break;
                }
                if let Some(caps) = Regex::new(r#"^\s*.depend\s*"(?P<m>\S+)"\s*("\w+")?\s*$"#)
                    .expect("Error: Fatal error in cllm, could not construct regex")
                    .captures(&line)
                {
                    let module_name = (&caps["m"]).to_string();
                    if !*compiled.get(&module_name).unwrap_or(&false) {
                        clean_dependencies.insert(module_name);
                    }
                } else if let Some(caps) = Regex::new(r#"^\s*.impobj\s*"(?P<obj>\S+)"\s*$"#)
                    .expect("Error: Fatal error in cllm, could not construct regex")
                    .captures(&line)
                {
                    cc_dependencies.insert(PathBuf::from((&caps["obj"]).to_string()));
                }
            }
            Err(_) => {
                eprintln!("Warning: Could not read line from abc file");
                continue;
            }
        }
    }
}

/// Create build directory if it doesn't exist
fn ensure_build_dir_exists() -> io::Result<()> {
    let path = Path::new("build/");
    if path.is_dir() {
        return Ok(());
    } else {
        create_dir(path)
    }
}

/// Builds the file, returns the location abc of the abc file
/// module_name is used to create abc file
fn build_file(search_path: &Vec<PathBuf>, module_name: &str) -> PathBuf {
    let joined_paths = search_path
        .iter()
        // For now, we only support Linux meaning that we can safely call to_str on a pathbuf
        .map(|p| p.to_str().unwrap())
        .collect::<Vec<&str>>()
        .join(":");

    let abc_out = &format!("build/{}.abc", module_name);

    Command::new("cocl-itasks")
        .arg("-wmt")
        .arg("-P")
        .arg(joined_paths)
        .arg("-o")
        .arg(abc_out)
        .arg(module_name)
        .stderr(Stdio::inherit())
        .stdout(Stdio::inherit())
        .output()
        .expect("failed to execute process");

    PathBuf::from(abc_out)
        .canonicalize()
        .expect(&format!("Could not canonicalize \"{}\"", abc_out))
}

/// Builds modules and adds its dependencies to the todo list. The map stores which modules have already been compiled
fn build_loop(
    search_paths: Vec<PathBuf>,
    todo: &mut BTreeSet<String>,
    cc_dependencies: &mut BTreeSet<PathBuf>,
) -> Vec<PathBuf> {
    let mut abc_files = Vec::new();
    let mut compiled: HashMap<String, bool> = HashMap::new();
    while !todo.is_empty() {
        if let Some(name) = todo.clone().iter().last() {
            let abc_path = build_file(&search_paths, &name);
            add_dependencies(&abc_path, todo, cc_dependencies, &mut compiled);
            compiled.insert(name.to_string(), true);
            abc_files.push(abc_path);
        } else {
            unreachable!();
        }
    }
    abc_files
}

/// Calls the code generator for every abc file, adds the resulting object files to the cc_dependencies
fn generate_code(abc_files: Vec<PathBuf>, cc_dependencies: &mut BTreeSet<PathBuf>) {
    for abc_file in abc_files {
        cc_dependencies.insert(abc_file.with_extension("o"));
        let abc_file = abc_file.with_extension("");
        Command::new("cg")
            .arg(abc_file)
            .stderr(Stdio::inherit())
            .stdout(Stdio::inherit())
            .output()
            .expect("failed to execute process");
    }
}

/// Pass all o and c files to gcc to link them into a single binary
fn link_object_files(cc_dependencies: BTreeSet<PathBuf>, output_path: &Path) {
    Command::new("cc")
        .args(cc_dependencies)
        .arg("-lc")
        .arg("-flto")
        .arg("-o")
        .arg(output_path)
        .stderr(Stdio::inherit())
        .stdout(Stdio::inherit())
        .output()
        .expect("failed to execute process");
}

/// Create the cgopts.c file
/// TODO: We want this function to return a PathBuf eventually. This would require modifying other
/// parts of the code such that we locate a file in the given path.
fn create_opts_file() -> io::Result<PathBuf> {
    let path = PathBuf::from("build/cgopts.c");
    let mut opts_file = File::create(&path)?;
    opts_file.write_all(b"long heap_size=2048<<10;")?;
    opts_file.write_all(b"long ab_stack_size=512<<10;")?;
    opts_file.write_all(b"long flags=8;")?;
    opts_file.write_all(b"long initial_heap_size=100<10;")?;
    opts_file.write_all(b"long heap_size_multiple=20<<8;")?;
    Ok(path)
}

/// Add the _startup.o and _system.o files to the vec of files that must be passed to cc.
fn add_rts_files(cc_dependencies: &mut BTreeSet<PathBuf>) {
    let key = "CLEAN_HOME";
    let path = env::var_os(key).expect("Could not read environment variable CLEAN_HOME");
    let startup = Path::new(&path).join("lib/StdEnv/Clean System Files/_startup.o");
    let system = Path::new(&path).join("lib/StdEnv/Clean System Files/_system.o");
    cc_dependencies.insert(startup);
    cc_dependencies.insert(system);
}

pub fn build_project(paths: Vec<PathBuf>, main_module: String, output: &Path) {
    ensure_build_dir_exists().expect("build/ does not exist and cannot be created");

    let mut cc_dependencies: BTreeSet<PathBuf> = BTreeSet::new();
    cc_dependencies.insert(create_opts_file().expect("Could not create cgopts.c file"));
    let mut todo: BTreeSet<String> = BTreeSet::new();
    todo.insert(main_module);

    add_rts_files(&mut cc_dependencies);

    let abc_files = build_loop(paths, &mut todo, &mut cc_dependencies);
    generate_code(abc_files, &mut cc_dependencies);

    link_object_files(cc_dependencies, output);
}
